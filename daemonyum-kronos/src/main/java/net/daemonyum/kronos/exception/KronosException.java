package net.daemonyum.kronos.exception;

@SuppressWarnings("serial")
public class KronosException extends Exception {

	public KronosException(String message) {
        super(message);
    }

    public KronosException(String message, Throwable cause) {
        super(message, cause);
    }
}

