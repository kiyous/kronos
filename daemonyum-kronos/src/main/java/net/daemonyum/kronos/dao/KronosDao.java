package net.daemonyum.kronos.dao;

import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;

import net.daemonyum.kronos.entity.KronosEntity;
import net.daemonyum.kronos.exception.KronosException;

public abstract class KronosDao<K extends KronosEntity> {
	
	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(KronosDao.class);
	
	protected SessionFactory sessionFactory;
	
	protected static KronosDao instance;
	
	protected KronosDao(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public abstract Class getEntityClass();
	
	public K findById(int id) {
		K entity = null;
		Session session = null;
			session = sessionFactory.openSession();			
			entity = (K)session.createCriteria(getEntityClass()).add(Restrictions.eq("id",id)).uniqueResult();
			
			LOGGER.debug(entity);

			
		
		return entity;	
	}
	
	@SuppressWarnings("unchecked")
	public List<K> findAll() throws KronosException {
		List<K> list = new LinkedList<K>();
		Session session = null;
		try {
			session = sessionFactory.openSession();	
			list = session.createCriteria(getEntityClass()).list();
		}catch(Exception e) {						
			throw new KronosException("",e);
		}finally {
			session.close();
		}	
		return list;
	}
	
	public void insert(K entity) throws KronosException {
		Session session = null;
		Transaction transaction = null;
		try {					
			session = sessionFactory.openSession();			
			transaction = session.beginTransaction();		
			session.save(entity);
			transaction.commit();
		}catch(Exception e) {			
			if(transaction != null) {
				transaction.rollback();
			}
			throw new KronosException("",e);			
		}finally {
			if(session != null) {
				session.close();
			}
		}
	}
	
	public void update(K entity) throws KronosException {
		Session session = null;
		Transaction transaction = null;
		try {					
			session = sessionFactory.openSession();			
			transaction = session.beginTransaction();		
			session.saveOrUpdate(entity);
			transaction.commit();
		}catch(Exception e) {			
			if(transaction != null) {
				transaction.rollback();
			}
			throw new KronosException("",e);
		}finally {
			if(session != null) {
				session.close();
			}
		}		
	}
	
	public void delete(K entity) throws KronosException {
		Session session = null;
		Transaction transaction = null;
		try {					
			session = sessionFactory.openSession();			
			transaction = session.beginTransaction();		
			session.delete(entity);
			transaction.commit();
		}catch(Exception e) {			
			if(transaction != null) {
				transaction.rollback();
			}
			throw new KronosException("",e);
		}finally {
			if(session != null) {
				session.close();
			}
		}		
	}

}
